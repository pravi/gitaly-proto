syntax = "proto3";

package gitaly;

import "shared.proto";

service SSHService {
  // To forward 'git upload-pack' to Gitaly for SSH sessions
  rpc SSHUploadPack(stream SSHUploadPackRequest) returns (stream SSHUploadPackResponse) {}

  // To forward 'git receive-pack' to Gitaly for SSH sessions
  rpc SSHReceivePack(stream SSHReceivePackRequest) returns (stream SSHReceivePackResponse) {}
}

message SSHUploadPackRequest {
  // 'repository' must be present in the first message.
  Repository repository = 1;
  // A chunk of raw data to be copied to 'git upload-pack' standard input
  bytes stdin = 2;
  // Prevent re-use of field id 3 and/or the "git_config_parameters" name
  reserved 3;
  reserved "git_config_parameters";
  // Parameters to use with git -c (key=value pairs)
  repeated string git_config_options = 4;
}

message SSHUploadPackResponse {
  // A chunk of raw data from 'git upload-pack' standard output
  bytes stdout = 1;
  // A chunk of raw data from 'git upload-pack' standard error
  bytes stderr = 2;
  // This field may be nil. This is intentional: only when the remote
  // command has finished can we return its exit status.
  ExitStatus exit_status = 3;
}

message SSHReceivePackRequest {
  // 'repository' must be present in the first message.
  Repository repository = 1;
  // A chunk of raw data to be copied to 'git upload-pack' standard input
  bytes stdin = 2;
  // Contents of GL_ID and GL_REPOSITORY environment variables for
  // 'git receive-pack'
  string gl_id = 3;
  string gl_repository = 4;
}

message SSHReceivePackResponse {
  // A chunk of raw data from 'git receive-pack' standard output
  bytes stdout = 1;
  // A chunk of raw data from 'git receive-pack' standard error
  bytes stderr = 2;
  // This field may be nil. This is intentional: only when the remote
  // command has finished can we return its exit status.
  ExitStatus exit_status = 3;
}
